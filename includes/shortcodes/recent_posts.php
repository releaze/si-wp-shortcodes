<?php
add_shortcode( 'recent_posts', 'si_ab_shortcodes_recent_posts_func' );

function si_ab_shortcodes_recent_posts_func( $atts, $content = '' ){
    extract( shortcode_atts( array(
        'post_to_show' => '3',
        'category'     => null,
        'show_date'    => 'yes',
        'show_excerpt' => 'no',
        'show_thumb'   => 'yes',
        'date_format' => 'j M Y',
    ), $atts ) );

    ob_start();

    $query = new WP_Query( array(
        'posts_per_page'      => $post_to_show,
        'category_name'       => $category,
        'no_found_rows'       => true,
        'post_status'         => 'publish',
        'ignore_sticky_posts' => true,
        'order' => 'DESC',
        'meta_key' => 'start_date',
        'orderby' => 'start_date',
    ) );

    $title = '';
    if ( ! empty($content) ) {
        $title = '<h2>' . $content . '</h2>';
    }

    if ( $query->have_posts() ) {?>
        <div id='si-ab-shortcodes-recent-posts' class='si-ab-shortcodes-recent-posts'>
            <?php echo $title; ?>
            <ul>
                <?php
                $index = 0;
                foreach ( $query->posts as $post ) :
                    $index++;
                    $post_title = get_the_title( $post->ID );
                    $title      = ( ! empty( $post_title ) ) ? $post_title : __( '(no title)' );
                    $meta       = get_post_meta( $post->ID );
                    $start_date = isset( $meta['start_date'] ) ? $meta['start_date'][0] : $post->post_date;
                    $date       = date_i18n( $date_format, strtotime( $start_date ) );

                    $post_excerpt    = $post->post_excerpt;
                    $thumbnail  = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'recent-posts-thumb' );
                    $image      = $thumbnail ? $thumbnail[0] : "";
                    $image_width  = $thumbnail ? $thumbnail[1] : "403";
                    $image_height = $thumbnail ? $thumbnail[2] : "250";

                    $time = '';
                    if ( $show_date == 'yes' ) {
                        $time = '<time>' . $date . '</time>';
                    }

                    $excerpt = '';
                    if ( $show_excerpt == 'yes' ) {
                        $excerpt = '<p>' . $post_excerpt . '</p>';
                    }
                    ?>
                    <li>
                        <figure>
                            <?php
                                if($show_thumb == 'yes') {
                            ?>
                                <div><img src=<?php echo $image; ?> width=<?php echo $image_width; ?> height=<?php echo $image_height; ?> alt="" onerror="this.style.visibility='hidden'"></div>
                             <?php } ?>
                            <figcaption>
                                <a href="<?php the_permalink( $post->ID ); ?>" class="post-title">
                                    <?php echo $title; ?>
                                </a>
                                <?php
                                echo $time;
                                echo $excerpt;
                                ?>
                            </figcaption>
                        </figure>
                    </li>
                <?php endforeach;
                wp_reset_postdata(); ?>
            </ul>
        </div>

        <?php
        $content = ob_get_clean();
        return $content;
    }
}

