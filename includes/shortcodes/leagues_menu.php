<?php
    add_shortcode( 'leagues_menu', 'si_ab_shortcodes_leagues_menu_func' );

	global $si_ab_ss2_page_data;
    function si_ab_shortcodes_leagues_menu_func( $atts, $content = '' ){
        $tournaments = si_ab_shortcodes_tournaments();
        $url         = get_site_url();

        extract( shortcode_atts( array(
            'sub_title'               => null,
            'url_prefix'              => '',
            'use_league_name_in_link' => 'no',
            'leagues'                 => '55,47,53,87,54,61,57',
            'national_leagues'        => '50,42,73,77',
        ), $atts ) );

        ob_start();

        $title = '';
        if ( ! empty($content) ) {
            $title = '<h2>' . $content . '</h2>';
        }

        if ( ! empty($sub_title) ) {
            $sub_title = '<h2>' . $sub_title . '</h2>';
        }
    ?>
            <div id='si_ab_shortcodes_leagues_menu' class='si_ab_shortcodes_leagues_menu' style=<?php echo $styles; ?>>
                <?php echo $title; ?>
                <ul>
                   <?php foreach ( explode(",", $leagues) as $id ) :
                    $name = $tournaments[intval($id)];
                    $label = $use_league_name_in_link == 'no' ? $id : $name;
                    $isSameId = $id == $ttId;
                    $className = $isSameId ? 'active' : '';
                   ?>
                        <li>
                            <a href=<?php echo $url  . "/" . $url_prefix . str_replace(' ', '-', strtolower($label)) ?> class=<?php echo "$className" ?>>
                                <picture>
                                    <img class="si-league-logo" alt=<?php echo $id; ?> src=<?php echo "https://dxugi372p6nmc.cloudfront.net/leagues/dark/64x64/$id.png"; ?>>
                                </picture>
                                <span><?php echo $name; ?></span>
                            </a>
                        </li>
                   <?php endforeach; ?>
               </ul>

               <?php echo $sub_title; ?>
                <ul>
                    <?php foreach ( explode(",", $national_leagues) as $id ) :
                        $name = $tournaments[intval($id)];
                        $label = $use_league_name_in_link == 'no' ? $id : $name;
                        $isSameId = $id == $ttId;
                        $className = $isSameId ? 'active' : '';
                        ?>
                        <li>
                            <a href=<?php echo $url  . "/" . $url_prefix . str_replace(' ', '-', strtolower($label)) ?> class=<?php echo "$className" ?>>
                                <picture>
                                    <img class="si-league-logo" alt=<?php echo $id; ?> src=<?php echo "https://dxugi372p6nmc.cloudfront.net/leagues/dark/64x64/$id.png"; ?>>
                                </picture>
                                <span><?php echo $name; ?></span>
                            </a>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>
    <?php
        $content = ob_get_clean();
        return $content;
    }
