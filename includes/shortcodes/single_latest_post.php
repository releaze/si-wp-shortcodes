<?php
add_shortcode( 'single_latest_post', 'si_ab_shortcodes_single_latest_post_func' );

function si_ab_shortcodes_single_latest_post_func( $atts, $content = '' ){
    extract( shortcode_atts( array(
        'button_text' => 'See Betting Tip',
        'category' => null,
        'show_ab_articles' => 'yes',
        'show_date' => 'yes',
        'show_excerpt' => 'no',
        'radius' => '0',
        'date_format' => 'j M Y',
    ), $atts ) );

    ob_start();

    $params = array(
        'posts_per_page' => 1,
        'category_name'  => $category,
        'post_type'      => 'post',
        'orderby'        => 'date',
        'order'          => 'ASC',
    );

    $dateNow = date("Y-m-d H:i:s");

    $query = new WP_Query( array(
        'posts_per_page'   => 1,
        'category_name'    => $category,
        'post_type'        => 'post',
        'meta_key'         => 'start_date',
        'orderby'          => 'start_date',
        'order'            => 'DESC',
        //Returns upcoming matches >= today date
        'meta_query' => array(
            'relation' => 'OR',
            array(
                'key'  => 'start_date',
                'value' => $dateNow,
                'type' => 'DATETIME',
                'compare' => '>=',
            )
        ),
    ));

    if ( $show_ab_articles == 'no' ) {
        $query = new WP_Query( array(
            'posts_per_page'      => 1,
            'category_name'       => $category,
            'no_found_rows'       => true,
            'post_status'         => 'publish',
            'ignore_sticky_posts' => true,
            'order'               => 'DESC',
        ) );
    }

    if ($query->post_count < 1) {
        $query = new WP_Query( array_merge($params, array( 'meta_query' => array( array( 'key' => 'start_date', 'compare' => 'NOT EXISTS' ) ) ) ) );
    }

    $title = '';
    if ( ! empty($content) ) {
        $title = '<h2>' . $content . '</h2>';
    }

    $styles = 'border-radius:' . esc_attr( $radius ) . 'px';

    if ( $query->have_posts() ) { ?>
        <div id="si-ab-shortcodes-single-latest-post" class='si-ab-shortcodes-single-latest-post'>
            <?php echo $title; ?>
            <?php
            $index = 0;
            foreach ( $query->posts as $post ) :
                $index++;
                $post_title   = $post->post_title;
                $title        = ( ! empty( $post_title ) ) ? $post_title : __( '(no title)' );
                $post_excerpt = $post->post_excerpt;
                $thumbnail    = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-latest-post-thumb' );
                $image        = $thumbnail ? $thumbnail[0] : "";
                $image_width  = $thumbnail ? $thumbnail[1] : "836";
                $image_height = $thumbnail ? $thumbnail[2] : "492";
                //$meta         = get_post_meta(get_the_ID());
                //$start_date   = isset($meta['start_date']) ? $meta['start_date'][0] : $post->post_date;
                $meta         = get_post_meta( $post->ID );
                $start_date   = isset( $meta['start_date'] ) ? $meta['start_date'][0] : $post->post_date;
                $date         = date_i18n( $date_format, strtotime( $start_date ) );

                $time = '';
                if ( $show_date == 'yes' ) {
                    $time = '<time>' . $date . '</time>';
                }

                $excerpt = '';
                if ( $show_excerpt == 'yes' ) {
                    $excerpt = '<p>' . $post_excerpt . '</p>';
                }

                ?>
                <div>
                    <figure>
                        <a href=<?php echo the_permalink( $post->ID ); ?> style=<?php echo $styles; ?>>
                            <picture>
                                <div><img src=<?php echo $image; ?> width=<?php echo $image_width; ?> height=<?php echo $image_height; ?>></div>
                            </picture>
                            <figcaption>
                                <h2><?php echo $title; ?></h2>
                                <div class="date-wrapper">
                                    <?php echo $time; ?>
                                    <span class="read-more"><?php echo $button_text ?> <i></i></span>
                                </div>
                                <?php echo $excerpt; ?>
                            </figcaption>
                        </a>
                    </figure>
                </div>
            <?php endforeach;
            wp_reset_postdata(); ?>
        </div>

        <?php
        $content = ob_get_clean();
        return $content;
    }
}

