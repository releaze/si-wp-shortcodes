<?php
add_shortcode( 'popular_news', 'si_ab_shortcodes_popular_news_func' );

function si_ab_shortcodes_popular_news_func( $atts, $content = '' ) {
    extract( shortcode_atts( array(
        'category' => null,
        'post_to_show' => '4',
        'post_with_thumbnail' => '1',
        'radius' => '0',
        'label_text' => 'New',
        'date_format' => 'j M Y',
    ), $atts ) );

    ob_start();

    $params = array(
        'posts_per_page' => $post_to_show,
        'category_name'  => $category,
        'post_type'      => 'post',
        'orderby'        => 'date',
        'order'          => 'DESC',
    );

    $query = new WP_Query( array(
        'posts_per_page'      => $post_to_show,
        'category_name'       => $category,
        'no_found_rows'       => true,
        'post_status'         => 'publish',
        'ignore_sticky_posts' => true,
        'order'               => 'DESC',

        'meta_key' => 'start_date',
        'orderby' => 'start_date',
    ) );

    if ($query->post_count < 1) {
        $query = new WP_Query( array_merge($params, array( 'meta_query' => array( array( 'key' => 'start_date', 'compare' => 'NOT EXISTS' ) ) ) ) );
    }

    $title = '';
    if ( ! empty($content) ) {
        $title = '<h2>' . $content . '</h2>';
    }

    $styles = 'border-radius:' . esc_attr( $radius ) . 'px';

    if ( $query->have_posts() ) { ?>
        <div id="si-ab-shortcodes-popular-news" class='si-ab-shortcodes-popular-news'>
            <?php echo $title; ?>
            <?php
            $index = 0;
            foreach ( $query->posts as $post ) :
                $index++;
                $post_title   = $post->post_title;
                $title        = ( ! empty( $post_title ) ) ? $post_title : __( '(no title)' );
                $post_excerpt = $post->post_excerpt;
                $thumbnail    = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'popular-news-thumb' );
                $image        = $thumbnail ? $thumbnail[0] : "";
                $image_width  = $thumbnail ? $thumbnail[1] : "403";
                $image_height = $thumbnail ? $thumbnail[2] : "250";

                $meta       = get_post_meta( $post->ID );
                $start_date = isset( $meta['start_date'] ) ? $meta['start_date'][0] : $post->post_date;
                $date       = date_i18n( $date_format, strtotime( $start_date ) );

                $time = '<time>' . $date . '</time>';
                ?>
                <div class="post">
                    <?php if ($index == $post_with_thumbnail) { ?>
                        <figure>
                            <a href=<?php echo the_permalink($post->ID); ?> style=<?php echo $styles; ?>>
                                <div class="label"><?php echo $label_text; ?></div>
                                <div class="img"><img src=<?php echo $image; ?> width=<?php echo $image_width; ?> height=<?php echo $image_height; ?>></div>
                                <figcaption>
                                    <div class="title"><?php echo mb_strimwidth($title, 0, 35, "..."); ?></div>
                                    <?php echo $time; ?>
                                    <p><?php echo mb_strimwidth($post_excerpt, 0, 80, "..."); ?></p>
                                </figcaption>
                            </a>
                        </figure>
                    <?php } else { ?>
                        <div class="title">
                            <a href=<?php echo the_permalink($post->ID); ?>>
                                <?php echo mb_strimwidth($title, 0, 60, "..."); ?>
                            </a>
                        </div>
                        <?php echo $time; ?>
                    <?php } ?>
                </div>
            <?php endforeach;
            wp_reset_postdata(); ?>
        </div>

        <?php
        $content = ob_get_clean();
        return $content;
    }
}
