<?php
add_shortcode('latest_posts', 'si_ab_shortcodes_latest_posts_func');

function si_ab_shortcodes_latest_posts_func($atts, $content = '')
{
    extract(shortcode_atts(array(
        'category' => null,
        'post_to_show' => '3',
        'show_ab_articles' => 'yes',
        'show_date' => 'yes',
        'radius' => '0',
        'skip_posts' => 0,
        'grid_columns' => 2,
        'date_format' => 'j M Y',
    ), $atts));

    ob_start();

    $dateNow = date("Y-m-d H:i:s");

    $query = new WP_Query(array(
        'posts_per_page' => $post_to_show,
        'category_name' => $category,
        'post_type' => 'post',
        'meta_key' => 'start_date',
        'orderby' => 'start_date',
        'order' => 'DESC',
        'offset' => $skip_posts,
        //Returns upcoming matches >= today date
        'meta_query' => array(
            'relation' => 'OR',
            array(
                'key' => 'start_date',
                'value' => $dateNow,
                'type' => 'DATETIME',
                'compare' => '>=',
            )
        ),
    ));

    if ($show_ab_articles == 'no') {
        $query = new WP_Query(array(
            'posts_per_page' => $post_to_show,
            'category_name' => $category,
            'no_found_rows' => true,
            'post_status' => 'publish',
            'ignore_sticky_posts' => true,
            'order' => 'DESC',
            'offset' => $skip_posts,
        ));
    }

    $title = '';
    if (!empty($content)) {
        $title = '<h2>' . $content . '</h2>';
    }

    $styles = 'border-radius:' . esc_attr($radius) . 'px';
    $grid = 'grid-template-columns:' . 'repeat(' . esc_attr($grid_columns) . ',' . '1fr)';

    if ($query->have_posts()) { ?>
        <div id='si-ab-shortcodes-latest-posts' class='si-ab-shortcodes-latest-posts'>
            <?php echo $title; ?>
            <div class="latest-posts-grid" style=<?php echo $grid; ?>>
                <?php
                $index = 0;
                foreach ($query->posts as $post) :
                    $index++;
                    $post_title    = $post->post_title;
                    $title         = (!empty($post_title)) ? $post_title : __('(no title)');
                    $thumbnail     = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'latest-post-thumb');
                    $image         = $thumbnail ? $thumbnail[0] : "";
                    $image_width   = $thumbnail ? $thumbnail[1] : "404";
                    $image_height  = $thumbnail ? $thumbnail[2] : "218";
                    //$meta          = get_post_meta(get_the_ID());
                    //$start_date = isset($meta['start_date']) ? $meta['start_date'][0] : $post->post_date;
                    $meta         = get_post_meta( $post->ID );
                    $start_date   = isset( $meta['start_date'] ) ? $meta['start_date'][0] : $post->post_date;
                    $date = date_i18n( $date_format, strtotime( $start_date ) );

                    $home_name = get_post_meta($post->ID, 'home_name', true);
                    $away_name = get_post_meta($post->ID, 'away_name', true);

                    $preparedTitle = ($home_name && $away_name) ? '' . $home_name . ' vs ' . $away_name . '' : $title;

                    $time = '';
                    if ( $show_date == 'yes' ) {
                        $time = '<time>' . $date . '</time>';
                    }
                    ?>
                    <figure>
                        <a href=<?php echo the_permalink($post->ID); ?> style=<?php echo $styles; ?>>
                            <div><img src=<?php echo $image; ?> width=<?php echo $image_width; ?> height=<?php echo $image_height; ?>></div>
                            <figcaption>
                                <h2><?php echo $preparedTitle; ?></h2>
                                <?php echo $time; ?>
                            </figcaption>
                        </a>
                    </figure>
                <?php endforeach;
                wp_reset_postdata(); ?>
            </div>
        </div>

        <?php
        $content = ob_get_clean();
        return $content;
    }
}
