<?php
add_action( 'after_setup_theme', 'si_ab_shortcodes_recent_posts_image_size' );
add_action( 'after_setup_theme', 'si_ab_shortcodes_single_latest_post_image_size' );
add_action( 'after_setup_theme', 'si_ab_shortcodes_latest_image_size' );
add_action( 'after_setup_theme', 'si_ab_shortcodes_popular_news_image_size' );

function si_ab_shortcodes_recent_posts_image_size(){
    add_image_size( 'recent-posts-thumb', 72, 72, true );
}

function si_ab_shortcodes_single_latest_post_image_size(){
    add_image_size( 'single-latest-post-thumb', 836, 492, true );
}

function si_ab_shortcodes_latest_image_size(){
    add_image_size( 'latest-post-thumb', 404, 217, true );
}

function si_ab_shortcodes_popular_news_image_size(){
    add_image_size( 'popular-news-thumb', 403, 250, true );
}
?>