<?php
/**
 * Plugin Name: SI AB Shortcodes
 * Description: The ability to use custom shortcodes
 * Version: 1.09
 * Requires at least: 5.8
 * Author:      Sports Innovation
 * Author URI:  https://sportsinnovation.dk
 * License:     GPLv2 or later
 * License URI: http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 * Domain Path: /languages
 * Text Domain: si_ab_shortcodes
 *
 */
if (!defined('ABSPATH')) {
    die;
}

/**
 * Register custom Settings page
 */
function si_ab_shortcodes_register_custom_page() {
    add_menu_page(
        esc_html__( 'Welcome to plugin page', 'si_ab_shortcodes' ),
        esc_html__( 'AB Shortcodes', 'si_ab_shortcodes' ),
        'manage_options',
        'si_ab_shortcodes_options',
        'si_ab_shortcodes_show_settings',
        'dashicons-shortcode',
        81
    );
}
add_action('admin_menu', 'si_ab_shortcodes_register_custom_page');

/**
 * Settings page body
 */
function si_ab_shortcodes_show_settings() {
    require plugin_dir_path( __FILE__ ) . 'assets/pages/settings-page.php';
}

/**
 * Load css for admin menu setting page
 */

function si_ab_shortcodes_load_assets($hook_suffix) {
    if ($hook_suffix != 'toplevel_page_si_ab_shortcodes_options') return;

    wp_enqueue_style( 'si_ab_shortcodes_styles', plugins_url( 'assets/css/admin.css', __FILE__ ) );
}
add_action( 'admin_enqueue_scripts', 'si_ab_shortcodes_load_assets', 99 );

/**
 * Load css and js for frontend
 */
function si_ab_shortcodes_load_frontend_assets() {
    wp_enqueue_style( 'si_ab_shortcodes', plugins_url( 'assets/css/styles.css', __FILE__ ) );
}
add_action( 'admin_enqueue_scripts', 'si_ab_shortcodes_load_frontend_assets', 100 );
add_action( 'wp_enqueue_scripts', 'si_ab_shortcodes_load_frontend_assets', 100 );

/**
 * Functions
 */
require plugin_dir_path( __FILE__ ) . 'functions.php';

/**
 * Helpers
 */
require plugin_dir_path( __FILE__ ) . 'helpers.php';

/**
 * Shortcodes
 */
require plugin_dir_path( __FILE__ ) . 'includes/shortcodes/single_latest_post.php';
require plugin_dir_path( __FILE__ ) . 'includes/shortcodes/latest_posts.php';
require plugin_dir_path( __FILE__ ) . 'includes/shortcodes/recent_posts.php';
require plugin_dir_path( __FILE__ ) . 'includes/shortcodes/error_page.php';
require plugin_dir_path( __FILE__ ) . 'includes/shortcodes/leagues_menu.php';
require plugin_dir_path( __FILE__ ) . 'includes/shortcodes/popular_news.php';


/**
 * Check update plugin
 */
//require plugin_dir_path( __FILE__ ) . 'plugin-update-checker/plugin-update-checker.php';
//$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
//    'https://bitbucket.org/releaze/si-wp-shortcodes/src/master/',
//    __FILE__,
//    'si_ab_shortcodes'
//);

//$myUpdateChecker->setAuthentication('si_ab_shortcodes_ssh_key');
//$myUpdateChecker->setBranch('master');
