<?php

/**
 * Return list of shortcodes.
 */
function si_ab_shortcodes_list()
{
    $shortcodes = [
        "single_latest_post" => array(
            'name' => 'Single Latest Post',
            'image' => plugins_url('assets/images/single-latest-post.svg', __FILE__),
            "example" => '[single_latest_post show_ab_articles="no" radius="10"]Single Latest Post[/single_latest_post]',
            "options" => array(
                'category' => array('possible_values' => 'Any text value', 'default_value' => null, "description_text" => ""),
                'button_text' => array('possible_values' => 'Any text value', 'default_value' => 'See Betting Tip', "description_text" => ""),
                'show_ab_articles' => array('possible_values' => 'yes or no', 'default_value' => 'yes', "description_text" => ""),
                'show_date' => array('possible_values' => 'yes or no', 'default_value' => 'yes', "description_text" => ""),
                'show_excerpt' => array('possible_values' => 'yes or no', 'default_value' => 'no', "description_text" => ""),
                "radius" => array('possible_values' => 'Number from 0 to 20', 'default_value' => '0', "description_text" => ""),
                "date_format" => array('possible_values' => 'Date format symbols', 'default_value' => 'j M Y', "description_text" => ""),
            )
        ),

        "latest_posts" => array(
            'name' => 'Latest Posts Grid',
            'image' => plugins_url('assets/images/latest-post-grid.svg', __FILE__),
            "example" => '[latest_posts post_to_show="4" show_ab_articles="no" radius="6" skip_posts="1"]Latest Posts[/latest_posts]',
            "options" => array(
                'category' => array('possible_values' => 'Any text value', 'default_value' => 'Uncategorized', "description_text" => ""),
                'show_ab_articles' => array('possible_values' => 'yes or no', 'default_value' => 'yes', "description_text" => ""),
                'show_date' => array('possible_values' => 'yes or no', 'default_value' => 'yes', "description_text" => ""),
                'skip_posts' => array('possible_values' => 'Number', 'default_value' => '0', "description_text" => "how many posts need to skip at the beginning"),
                'post_to_show' => array('possible_values' => 'Number from 1 to 30', 'default_value' => '3', "description_text" => "how many posts need to show"),
                "radius" => array('possible_values' => 'Number from 0 to 20', 'default_value' => '0', "description_text" => "corner radius"),
                "grid_columns" => array('possible_values' => 'Number', 'default_value' => '2', "description_text" => "how many posts to display in the grid"),
                "date_format" => array('possible_values' => 'Date format symbols', 'default_value' => 'j M Y', "description_text" => ""),
            ),
        ),

        "recent_posts" => array(
            'name' => 'Recent Posts',
            'image' => plugins_url('assets/images/recent-posts.svg', __FILE__),
            "example" => '[recent_posts post_to_show="3" show_date="yes"]Recent Posts[/recent_posts]',
            "options" => array(
                'category' => array('possible_values' => 'Any text value', 'default_value' => null, "description_text" => ""),
                'post_to_show' => array('possible_values' => 'Number from 1 to 30', 'default_value' => '3', "description_text" => ""),
                'show_date' => array('possible_values' => 'yes or no', 'default_value' => 'yes', "description_text" => ""),
                'show_thumb' => array('possible_values' => 'yes or no', 'default_value' => 'yes', "description_text" => ""),
                'show_excerpt' => array('possible_values' => 'yes or no', 'default_value' => 'no', "description_text" => ""),
                "date_format" => array('possible_values' => 'Date format symbols', 'default_value' => 'j M Y', "description_text" => ""),
            )
        ),

        "error_page" => array(
            'name' => 'Error Page',
            'image' => plugins_url('assets/images/error.svg', __FILE__),
            "example" => '[error_page description="It looks like you&apos;re lost" color="#e0e5e6" icon_color="#3497da"]404 Error Page[/error_page]',
            "options" => array(
                'description' => array('possible_values' => 'Any text value', 'default_value' => null, "description_text" => ""),
                'color' => array('possible_values' => 'Color text value', 'default_value' => "#ccc", "description_text" => ""),
                'icon_color' => array('possible_values' => 'Color text value', 'default_value' => "#ccc", "description_text" => ""),
                'button_text' => array('possible_values' => 'Any text value', 'default_value' => 'go back home', "description_text" => ""),
            )
        ),

        "leagues_menu" => array(
            'name' => 'League Menu',
            'image' => plugins_url('assets/images/leagues_menu.svg', __FILE__),
            "example" => '[leagues_menu sub_title="International Competitions" url_prefix="quote"]Tournaments[/leagues_menu]',
            "options" => array(
                'sub_title' => array('possible_values' => 'Any text value', 'default_value' => null, "description_text" => ""),
                'url_prefix' => array('possible_values' => 'Any text value', 'default_value' => '', "description_text" => ""),
                'use_league_name_in_link' => array('possible_values' => 'yes or no', 'default_value' => 'no', "description_text" => ""),
                'leagues' => array('possible_values' => 'String with numbers separated by commas', 'default_value' => '55,47,53,87,54,61,57', "description_text" => ""),
                'national_leagues' => array('possible_values' => 'String with numbers separated by commas', 'default_value' => '50,42,73,77', "description_text" => ""),
            )
        ),

        "popular_news" => array(
            'name' => 'Popular News',
            'image' => plugins_url('assets/images/popular-news.svg', __FILE__),
            "example" => '[popular_news]Popular News[/popular_news]',
            "options" => array(
                'category' => array('possible_values' => 'Any text value', 'default_value' => null, "description_text" => ""),
                'post_to_show' => array('possible_values' => 'Number from 1 to 30', 'default_value' => '4', "description_text" => "how many posts need to show"),
                'label_text' => array('possible_values' => 'Any text value', 'default_value' => 'New', "description_text" => ""),
                'post_with_thumbnail' => array('possible_values' => 'Number from 1 to ($post_to_show) value', 'default_value' => '1', "description_text" => "number of post with picture"),
                "radius" => array('possible_values' => 'Number from 0 to 20', 'default_value' => '0', "description_text" => "corner radius"),
                "date_format" => array('possible_values' => 'Date format symbols', 'default_value' => 'j M Y', "description_text" => ""),
            )
        ),

//        "advanced_carousel" => array(
//            'name' => 'Advanced Carousel',
//            'image' => plugins_url( 'assets/images/advanced-carousel.svg' , __FILE__ ),
//            "example" => '[advanced_carousel]Advanced Carousel[/advanced_carousel]',
//            "options" => array(
//                'category' => array('possible_values' => 'Any text value', 'default_value' => null, "description_text" => ""),
//            )
//        ),
//

//        "animated_box" => array(
//            'name' => 'Animated Box',
//            'image' => plugins_url( 'assets/images/animated-box.svg' , __FILE__ ),
//            "example" => '[animated_box]Animated Box[/animated_box]',
//            "options" => array(
//                'category' => array('possible_values' => 'Any text value', 'default_value' => null, "description_text" => ""),
//            )
//        ),
    ];

    return $shortcodes;
}

function si_ab_shortcodes_tournaments()
{
    $tournaments = [
        42 => "Champions League",
        46 => "3F Superleague",
        47 => "Premier League",
        48 => "England Championship",
        50 => "European Championship",
        53 => "Ligue 1",
        54 => "1 Bundesliga",
        55 => "Serie A",
        57 => "Eredivisie",
        59 => "Eliteserien",
        61 => "Primeira Liga",
        67 => "Allsvenskan",
        71 => "Super Lig",
        73 => "Europa League",
        76 => "World Cup",
        77 => "Coppa del mondo",
        85 => "Division 1",
        87 => "La Liga",
        108 => "League One",
        109 => "League Two",
        132 => "FA Cup",
        133 => "EFL Cup",
        138 => "Copa del Rey",
        141 => "Coppa Italia",
        146 => "Bundesliga",
        239 => "Division 2",
        245 => "Superligaen Qualification",
        9375 => "Champions League",
        9806 => "UEFA Nations League A",
        9807 => "UEFA Nations League B",
        9808 => "UEFA Nations League C",
        9809 => "UEFA Nations League D",
    ];

    return $tournaments;
}

