<?php
    $shortcodePage = esc_attr( $_GET['shortcode'] );
?>

<div class="si-ab-shortcodes-settings">
    <h1>SI AB Shortcodes</h1>

     <?php
        if ( empty($shortcodePage) ) {
        	include 'shortcodes-list.php';
        } else {
        	include 'shortcode-single-page.php';
        }
     ?>
</div>
