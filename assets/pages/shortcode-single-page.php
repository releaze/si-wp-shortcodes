<?php
$page             = esc_attr( $_GET['shortcode'] );
$shortcodes       = si_ab_shortcodes_list();
$shortcodeName    = $shortcodes[$page]['name'];
$example          = $shortcodes[$page]['example'];
$settingsPageURL  = admin_url('admin.php?page=si_ab_shortcodes_options');
$shortcodeOptions = $shortcodes[$page]['options'];
?>

<div class="si-ab-shortcodes-single-page">
    <h2>Shortcode: <b><?php echo $shortcodeName; ?></b></h2>

    <div class="si-ab-shortcodes-single-section back">
        <a href=<?php echo $settingsPageURL; ?>>
            <span class="dashicons dashicons-arrow-left-alt"></span>
            <?php echo esc_html__( 'Back to shortcodes list', 'si_ab_shortcodes' ); ?>
        </a>
    </div>

    <section class="si-ab-shortcodes-single-section preview">
        <h3>Default preview</h3>
        <div class="preview-content">
            <?php echo do_shortcode( $example ); ?>
        </div>
    </section>

    <section class="si-ab-shortcodes-single-section shortcode">
        <h3>Shortcode</h3>
        <pre contenteditable="true" class=""><code><?php echo $example; ?></code></pre>
    </section>

    <section class="si-ab-shortcodes-single-section options">
        <h3>Shortcode options</h3>

        <table class="">
            <tbody>
            <tr>
                <th>Option name</th>
                <th>Possible values</th>
                <th>Default value</th>
            </tr>
            <?php foreach ( $shortcodeOptions as $key => $values ) :
                $name = $key;
                $value = $values['possible_values'];
                $default = $values['default_value'];
                $description = $values['description_text'];
                ?>
                <tr>
                    <td><b><?php echo $name; ?></b><br/><small><?php echo $description; ?></small></td>
                    <td><?php echo $value; ?></td>
                    <td><?php echo $default; ?></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </section>

    <div class="si-ab-shortcodes-single-section back">
        <a href=<?php echo $settingsPageURL; ?>>
            <span class="dashicons dashicons-arrow-left-alt"></span>
            <?php echo esc_html__( 'Back to shortcodes list', 'si_ab_shortcodes' ); ?>
        </a>
    </div>
</div>
