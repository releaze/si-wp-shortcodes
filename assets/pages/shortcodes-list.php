<?php
    $shortcodes = si_ab_shortcodes_list();
?>

<h4><?php echo esc_html__( 'Available shortcodes', 'si_ab_shortcodes' );?></h4>
<ul class="si-ab-shortcodes-list">
    <?php foreach ( $shortcodes as $key => $values ) :
        $url = esc_attr(add_query_arg( 'shortcode', $key, admin_url('admin.php?page=si_ab_shortcodes_options')));
    ?>
        <li>
            <figure>
                <a href=<?php echo $url; ?>>
                    <img src=<?php echo $values['image']; ?>>
                </a>
                <figcaption>
                    <a href=<?php echo $url; ?>><?php echo $values['name']; ?></a>
                </figcaption>
            </figure>
        </li>
    <?php endforeach; ?>
</ul>
